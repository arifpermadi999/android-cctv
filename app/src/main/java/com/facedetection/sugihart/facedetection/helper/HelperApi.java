package com.facedetection.sugihart.facedetection.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HelperApi {

    public static boolean isOnline(@NonNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        // if running on emulator return true always.
        return android.os.Build.MODEL.equals("google_sdk");
    }
    public String GetApi(final Context ctx, FormBody body, String LinkApi){
        OkHttpClient client = new OkHttpClient();
        client.cache();

        SharedPreferences pref = ctx.getSharedPreferences("pref_face_detection",0);
        String TokenSession = pref.getString("token", null);
        String base_url_ip = pref.getString("base_url", null);

        //Toast.makeText(this, TokenSession, Toast.LENGTH_SHORT).show();

        Request req = new Request.Builder()
                .url(base_url_ip + LinkApi)
                .post(body)
                .addHeader("Authorization","Bearer " + TokenSession)
                .addHeader("Accept","application/json")
                .build();
        final String[] responsebody = new String[1];
        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                
                ((Activity)ctx).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        responsebody[0] = response.body().toString();
                    }
                });
            }
        });
        return responsebody[0];
    }

}
