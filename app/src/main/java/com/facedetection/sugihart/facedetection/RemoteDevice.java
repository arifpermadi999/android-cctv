package com.facedetection.sugihart.facedetection;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RemoteDevice extends AppCompatActivity {
    ImageView list_add;
    GridView grid;
    public static RemoteDevice RD;
    SharedPreferences pref;

    String[] device_name;
    String[] device_link;
    boolean list_click = false;
    ImageView scan_barcode;

    void init(){
        RD = this;
        list_add = (ImageView) findViewById(R.id.list_add);
        grid = (GridView) findViewById(R.id.grid);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_device);
        try{

            init();
            getDataDevice();
            list_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String[] colors = {"Manual", "QR CODE"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(RemoteDevice.this);
                    builder.setTitle("Option");
                    builder.setItems(colors, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(which == 0){

                                Intent in = new Intent(getApplicationContext(),RemoteDeviceAddEdit.class);
                                in.putExtra("form","Add");
                                in.putExtra("device_link","");
                                startActivity(in);
                            }else if(which == 1){

                                Intent in = new Intent(getApplicationContext(),QRCode.class);
                                in.putExtra("form","Device");
                                startActivity(in);
                            }
                        }
                    });

                    builder.show();

                }
            });

        }catch (Exception ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
    public void getDataDevice(){

        pref = getApplicationContext().getSharedPreferences("pref_list_device",0);
        int n_device = pref.getAll().size() - 1 ;
        n_device = n_device / 2;

        device_name = new String[n_device];
        device_link = new String[n_device];
        int i = 0;
        int j = 0;

        Map<String,?> entries = pref.getAll();
        Set<String> keys = entries.keySet();
        String[] keyss = new String[keys.size()];
        int k = 0;
        for (String key : keys) {
            keyss[k] = key;
            k++;
        }
        for(int m=keyss.length - 1;m >= 0;m--){
            try{
                String key = keyss[m];
                if(key.substring(0,11).equals("device_name")){
                    device_name[i] = pref.getString(key,null);
                    i+=1;
                }
                if(key.substring(0,11).equals("device_link")){
                    device_link[j] = pref.getString(key,null);
                    j+=1;
                }

            }catch (Exception ex){

            }
        }

        grid.setAdapter(new RemoteDeviceAdapter(getApplicationContext()));
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!list_click) {
                    Intent in = new Intent(getApplicationContext(), DeviceOnOff.class);
                    in.putExtra("url", device_link[position]);
                    startActivity(in);
                }
            }
        });
        grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                list_click =true;
                try{
                    String[] colors = {"Edit", "Delete"};

                    grid.setItemChecked(position, true);
                    AlertDialog.Builder builder = new AlertDialog.Builder(RemoteDevice.this);
                    builder.setTitle("Option");
                    builder.setItems(colors, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(which == 0){

                                list_click = false;
                                Intent in = new Intent(getApplicationContext(),RemoteDeviceAddEdit.class);
                                in.putExtra("form","Update");
                                in.putExtra("id_scene",position + 1);
                                startActivity(in);
                            }else if(which == 1){

                                new SweetAlertDialog(RemoteDevice.this, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Apakah Anda Yakin?")
                                        .setContentText("Jika iya data anda akan hilang selamanya")
                                        .setConfirmText("Iya")
                                        .setCancelText("Tidak")
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                list_click = false;
                                                sweetAlertDialog.dismissWithAnimation();
                                            }
                                        })
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                int pos = position + 1;
                                                pref.edit().remove("device_name" + pos).commit();
                                                pref.edit().remove("device_link" + pos).commit();
                                                pref.edit().remove("posN" + pos).commit();
                                                int n_device2 = pref.getInt("n_device",0) - 1;
                                                pref.edit().putInt("n_device",n_device2).commit();
                                                list_click = false;
                                                sDialog.dismissWithAnimation();
                                                getDataDevice();
                                                new SweetAlertDialog(RemoteDevice.this, SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Sukses")
                                                        .setContentText("Berhasil Menghapus Data!!")
                                                        .setConfirmText("Oke")
                                                        .show();
                                            }
                                        })
                                        .show();
                            }
                        }
                    });

                    builder.show();
                }catch (Exception ex){

                }

                return false;
            }
        });

        //SharedPreferences.Editor editor = pref.edit();
    }
    public class RemoteDeviceAdapter extends ArrayAdapter<String> {

        public RemoteDeviceAdapter(Context ctx) {
            super(ctx, R.layout.item_remote_device, device_name);
        }
        public View getView(int position , View cView, ViewGroup parent){
            View row = cView;
            if(row == null){
                LayoutInflater lay = getLayoutInflater();
                row = lay.inflate(R.layout.item_remote_device,parent,false);
            }
            //ImageView img_user = (ImageView) row.findViewById(R.id.img_user);
            TextView txtname = (TextView) row.findViewById(R.id.txtname);

            txtname.setText(device_name[position]);

            return row;
        }
    }
}
