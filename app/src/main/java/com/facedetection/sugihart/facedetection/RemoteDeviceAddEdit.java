package com.facedetection.sugihart.facedetection;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import cn.pedant.SweetAlert.SweetAlertDialog;

public class RemoteDeviceAddEdit extends AppCompatActivity {

    ImageView scan_barcode;
    public static EditText device_link,device_name;
    Button btn_add_edit;
    String form;
    int n_device = 0;
    SharedPreferences pref;
    public static RemoteDeviceAddEdit RDAE;
    Spinner spinner;
    String icon_name[] = {"Lampu","TV","AC"};
    int icon_img[] = {R.drawable.i2_home,R.drawable.i2_profile,R.drawable.i2_live};

    void init(){
        RDAE = this;
        pref = getApplicationContext().getSharedPreferences("pref_list_device",0);

        form = getIntent().getStringExtra("form").toString();

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(new IconDeviceAdapter(this));
        device_link = (EditText) findViewById(R.id.device_link);
        device_name = (EditText) findViewById(R.id.device_name);
        btn_add_edit = (Button) findViewById(R.id.btn_add_edit);
        n_device = pref.getInt("n_device",0);
        scan_barcode = (ImageView) findViewById(R.id.scan_barcode);
        scan_barcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(),QRCode.class);
                in.putExtra("form","EditAddDevice");
                startActivity(in);
            }
        });

        try{

            if(form.equals("Update")){
                int id_device = getIntent().getIntExtra("id_device",0);

                device_name.setText(pref.getString("device_name" + String.valueOf(id_device),null).toString());
                device_link.setText(pref.getString("device_link" + String.valueOf(id_device),null).toString());
            }

            if(form.equals("Add")){
                device_link.setText(getIntent().getStringExtra("device_link").toString());
            }
        }catch (Exception ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_device_add_edit);
        init();

        try{
            getSupportActionBar().setTitle(form + " Device");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_black_small);
        }catch (Exception ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        btn_add_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(form.equals("Add")) {
                    try{
                        final SharedPreferences.Editor editor = pref.edit();
                        n_device +=1;
                        new SweetAlertDialog(RemoteDeviceAddEdit.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Success")
                                .setContentText(form + " Data Success :)")
                                .setConfirmText("Oke")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        //startActivity(new Intent(getApplicationContext(),UserList.class));
                                        editor.putString("device_name" + n_device,device_name.getText().toString());
                                        editor.putString("device_link" + n_device,device_link.getText().toString());
                                        editor.putInt("n_device",n_device);
                                        editor.commit();
                                        RemoteDevice.RD.getDataDevice();
                                        finish();
                                    }
                                }).show();
                    }catch (Exception ex){
                        Toast.makeText(RemoteDeviceAddEdit.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }else{

                    try{
                        final SharedPreferences.Editor editor = pref.edit();
                        n_device = getIntent().getIntExtra("id_device",0);
                        new SweetAlertDialog(RemoteDeviceAddEdit.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Success")
                                .setContentText(form + " Data Success :)")
                                .setConfirmText("Oke")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                                        editor.putString("device_name" + n_device,device_name.getText().toString());
                                        editor.putString("device_link" + n_device,device_link.getText().toString());
                                        editor.commit();
                                        RemoteDevice.RD.getDataDevice();
                                        finish();
                                    }
                                }).show();
                    }catch (Exception ex){
                        Toast.makeText(RemoteDeviceAddEdit.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public class IconDeviceAdapter extends ArrayAdapter<String> {
        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        public IconDeviceAdapter(Context ctx) {
            super(ctx, R.layout.item_icon_device, icon_name);
        }
        public View getView(int position , View cView, ViewGroup parent){
            View row = cView;
            if(row == null){
                LayoutInflater lay = getLayoutInflater();
                row = lay.inflate(R.layout.item_icon_device,parent,false);
            }
            ImageView img_icon = (ImageView) row.findViewById(R.id.img);
            TextView txtname = (TextView) row.findViewById(R.id.txt);

            txtname.setText(icon_name[position]);
            img_icon.setImageResource(icon_img[position]);

            return row;
        }
    }
}
