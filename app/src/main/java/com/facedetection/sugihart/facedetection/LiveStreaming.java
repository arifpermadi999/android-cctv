package com.facedetection.sugihart.facedetection;

import android.content.res.Configuration;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.vision.text.Line;
import com.skyfishjy.library.RippleBackground;

public class LiveStreaming extends AppCompatActivity {
    VideoView videoView;
    public static LiveStreaming ls;
    LinearLayout live_streaming_layout;
    RippleBackground rippleBackground;
    ImageView btnup,btndown,btnleft,btnright,btnspeaker;
    Boolean speakerOn = false;

    public void init(){
        live_streaming_layout = (LinearLayout) findViewById(R.id.live_streaming_layout);
        btnup = (ImageView) findViewById(R.id.btn_speaker);
        btndown = (ImageView) findViewById(R.id.btn_bottom);
        btnleft = (ImageView) findViewById(R.id.btn_left);
        btnright = (ImageView) findViewById(R.id.btn_right);
        btnspeaker = (ImageView) findViewById(R.id.btn_speaker);
        videoView = (VideoView) findViewById(R.id.video_view);
        rippleBackground=(RippleBackground)findViewById(R.id.content);
        ls = this;

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_streaming);
        init();
        btnspeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!speakerOn){
                    Toast.makeText(LiveStreaming.this, "Speaker Aktif", Toast.LENGTH_SHORT).show();
                    rippleBackground.startRippleAnimation();
                    speakerOn = true;
                }else{
                    Toast.makeText(LiveStreaming.this, "Speaker Tidak Aktif", Toast.LENGTH_SHORT).show();
                    speakerOn  = false;
                    rippleBackground.stopRippleAnimation();
                }
            }
        });
        try{
            getSupportActionBar().setTitle("Live Streaming");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }catch (Exception ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        //RtspStream();
        getWebView(getIntent().getStringExtra("url").toString());
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Toast.makeText(getApplicationContext(), String.valueOf(newConfig.orientation), Toast.LENGTH_SHORT).show();
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(getApplicationContext(), "Landscape", Toast.LENGTH_SHORT).show();
            live_streaming_layout.setOrientation(LinearLayout.HORIZONTAL);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(getApplicationContext(), "Potrait", Toast.LENGTH_SHORT).show();
            live_streaming_layout.setOrientation(LinearLayout.VERTICAL);
        }
    }
    public void getWebView(String url){
/*
        WebView webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);

        // Tiga baris di bawah ini agar laman yang dimuat dapat
        // melakukan zoom.
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        // Baris di bawah untuk menambahkan scrollbar di dalam WebView-nya
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);*/
    }
    public void RtspStream() {
        videoView.setVideoURI(Uri.parse("http://122844270777.ip-dynamic.com:8080/CVVotFBf6mmTAksEt8vfxqL7v5aAUi/mp4/H3naAOyoiW/jbP7Zj87oU/s.mp4"));
        videoView.requestFocus();
        videoView.start();
    }
}
